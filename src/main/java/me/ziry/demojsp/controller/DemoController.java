package me.ziry.demojsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {

    @GetMapping("/index")
    public String index(Model model){
        model.addAttribute("name", "测试添加jsp");
        return "index";
    }

}
