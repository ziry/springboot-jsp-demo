## 介绍

SpringBoot整合使用jsp，与Thymeleaf类似，只是jsp只是多了webapp这个目录用来存放jsp的目录，静态资源还是放在resources的static下面。

## 目录结构
![](image.png)

## 引入依赖

```XML
<!--WEB支持-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
 
<!--jsp页面使用jstl标签-->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
</dependency>
 
<!--用于编译jsp-->
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-jasper</artifactId>
    <!--
     **有添加provided的情况：**
      - 右键运行启动类，访问页面报404错误
      - 使用spring-boot:run运行正常
      - 打包成jar，通过 java -jar demojsp-0.0.1-SNAPSHOT.jar 运行报错
      - 打包成war，通过 java -jar demojsp-0.0.1-SNAPSHOT.war 运行正常

      **把provided 注释掉的情况**
      - 右键运行启动类，访问页面正常
      - spring-boot:run运行 访问页面正常
      - 打包成jar，通过 java -jar demojsp-0.0.1-SNAPSHOT.jar 运行报错
      - 打包成war，通过 java -jar demojsp-0.0.1-SNAPSHOT.war 运行正常
    -->
    <scope>provided</scope>
</dependency>
```



## 配置文件(resources/application.yml)

```YAML
spring:
  mvc:
    view:
      prefix: /WEB-INF/jsp/
      suffix: .jsp
```



## Controller(me.ziry.demojsp.controller.DemoController)

```Java
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {

    @GetMapping("/index")
    public String index(Model model){
        model.addAttribute("name", "测试添加jsp");
        return "index";
    }

}

```



## JSP(webapp/WEB-INF/jsp/index.jsp)

```HTML
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOC TYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Demo jsp</title>
</head>
<body>
    内容：${name}
</body>
</html>
```





